# MapLib

#### 介绍
地图框架封装

####  使用说明

1.  权限配置
```
    <!--网络-->
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <!--存储-->
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <!--定位-->
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```
2.  AndroidManifest.xml配置
```
         <!--高德地图key,在高德开发平台申请-->
        <meta-data
            android:name="com.amap.api.v2.apikey"
            android:value="532975e1944c6dc4353a264df2d05afd"/>
        <!--高德定位需要的service-->
        <service android:name="com.amap.api.location.APSService"/>
```
3.  Application中初始化
```
    MapInit.init(Application application)
```






