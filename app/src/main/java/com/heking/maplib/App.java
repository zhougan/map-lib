package com.heking.maplib;

import android.app.Application;
import android.util.Log;

import com.zhoug.map.MapUtils;

/**
 * @Author 35574
 * @Date 2021/9/14
 * @Description
 */
public class App extends Application {
    private static final String TAG = ">>>App";

    @Override
    public void onCreate() {
        super.onCreate();
//        getParentCode("");
//        getParentCode(null);
//        getParentCode("110000000000");
//        getParentCode("110000");
//        getParentCode("11");
//        getParentCode("450000000000");
//        getParentCode("450000");
//        getParentCode("450100000000");
//        getParentCode("450102000000");
//        getParentCode("450102001000");
//        getParentCode("450102001003");
    }

    private void getParentCode(String areaCode){
        Log.d(TAG, areaCode+"->"+ MapUtils.getParentAreaCode(areaCode));
    }


}
