package com.heking.maplib.base;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;

/**
 * 基类Activity
 * @Author 35574
 * @Date 2021/9/14
 * @Description
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected static String TAG = ">>>BaseActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        TAG=">>>"+getClass().getSimpleName();
        findViews();
        addListeners();
        init(savedInstanceState);
    }

    protected abstract @LayoutRes
    int getLayoutResId();

    protected abstract void findViews();

    protected abstract void addListeners();

    protected abstract void init(Bundle savedInstanceState);

}



