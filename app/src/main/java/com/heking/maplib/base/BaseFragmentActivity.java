package com.heking.maplib.base;

import android.os.Bundle;

import com.heking.maplib.R;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * 基类嵌入Fragment的Activity
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public abstract class BaseFragmentActivity extends BaseActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_activity;
    }

    @Override
    protected void findViews() {

    }

    @Override
    protected void addListeners() {

    }

    @Override
    protected void init(Bundle savedInstanceState) {
        FragmentManager sfm = getSupportFragmentManager();
        FragmentTransaction ft = sfm.beginTransaction();
        Fragment fragment = getFragment();
        ft.replace(R.id.fragment_container,fragment,fragment.getClass().getCanonicalName());
        ft.commit();
    }


    protected abstract Fragment getFragment();

}
