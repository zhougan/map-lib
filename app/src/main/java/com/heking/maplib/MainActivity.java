package com.heking.maplib;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.heking.maplib.base.BaseActivity;
import com.heking.maplib.pages.FragmentMapActivity;
import com.heking.maplib.pages.MapActivity;
import com.zhoug.permissions.ZPermissions;

public class MainActivity extends BaseActivity implements View.OnClickListener{
    private LinearLayout mContainer;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void findViews() {
        mContainer = findViewById(R.id.container);
    }

    @Override
    protected void addListeners() {
        int childCount = mContainer.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = mContainer.getChildAt(i);
            child.setClickable(true);
            child.setOnClickListener(this);
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ZPermissions.with(this)
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                        )
                .setCallback((success,granted,dened)->{
                    if(success){

                    }
                }).request();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.btn_MapActivity:
                Router.navigation(this, MapActivity.class);
                break;
            case R.id.btn_FragmentMapActivity:
                Router.navigation(this, FragmentMapActivity.class);
                break;
        }
    }









}