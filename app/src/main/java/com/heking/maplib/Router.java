package com.heking.maplib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

/**
 * @Author 35574
 * @Date 2021/9/14
 * @Description
 */
public class Router {

    public static void navigation(@NonNull Context context, @NonNull Class<? extends Activity> activityClass, Bundle args) {
        Intent intent = new Intent(context, activityClass);
        if (args != null) {
            intent.putExtras(args);
        }
        context.startActivity(intent);
    }

    public static void navigation(@NonNull Context context, @NonNull Class<? extends Activity> activityClass) {
        navigation(context, activityClass, null);
    }

}
