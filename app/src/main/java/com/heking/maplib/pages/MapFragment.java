package com.heking.maplib.pages;

import android.os.Bundle;
import android.view.View;

import com.amap.api.maps.MapView;
import com.heking.maplib.R;
import com.heking.maplib.base.BaseFragment;
import com.zhoug.map.amap.AMapLifecycleHelper;

import androidx.annotation.NonNull;

/**
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class MapFragment extends BaseFragment {
    private static final String TAG = ">>>MapFragment";
    private AMapLifecycleHelper aMapLifecycleHelper;
    private MapView mMapView;

    @Override
    protected int getLayoutResId() {
        return R.layout.map_fragment;
    }

    @Override
    protected void findViews(View view) {
        mMapView = view.findViewById(R.id.mapView);
    }

    @Override
    protected void initView(View root, Bundle savedInstanceState) {
        aMapLifecycleHelper = new AMapLifecycleHelper(this, mMapView, savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        aMapLifecycleHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        aMapLifecycleHelper.onDestroyView();
    }

}
