package com.heking.maplib.pages;

import com.heking.maplib.base.BaseFragmentActivity;

import androidx.fragment.app.Fragment;

/**
 *
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class FragmentMapActivity extends BaseFragmentActivity {


    @Override
    protected Fragment getFragment() {
        return new MapFragment();
    }
}
