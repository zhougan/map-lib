package com.heking.maplib.pages;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.maps.model.Polygon;
import com.amap.api.maps.model.PolygonOptions;
import com.amap.api.maps.model.VisibleRegion;
import com.heking.maplib.R;
import com.heking.maplib.base.BaseActivity;
import com.zhoug.map.amap.AMapHelper;
import com.zhoug.map.amap.AMapLifecycleHelper;
import com.zhoug.map.amap.MapProvinceShowLimitHelper;
import com.zhoug.map.amap.MarkerOptionsBuild;
import com.zhoug.map.amap.MarkerUtils;
import com.zhoug.map.amap.MyLocationHelper;
import com.zhoug.map.amap.MyLocationStyleBuild;
import com.zhoug.map.data.database.MapDatabase;
import com.zhoug.map.data.database.dao.BoundaryDao;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;

/**
 * @Author 35574
 * @Date 2021/9/14
 * @Description
 */
public class MapActivity extends BaseActivity {
    private static final String TAG = ">>>MapActivity";
    private static boolean DEBUG = true;
    private ThreadPoolExecutor mThreadPoolExecutor;

    private MapView mMapView;
    private AMap mAMap;
    private AMapLifecycleHelper aMapLifecycleHelper;
    private Button mBtnBound;
    private Button mBtnDeleteMarker;
    private Marker marker;
    private Button mBtnOverlay;
    private Button mBtnOnly;
    private EditText mEtCity;
    private Button mBtnOnly2;
    private MapProvinceShowLimitHelper mMapShadowMaskHelper;
    private Button mBtnSc;
    private Button mBtnCq;
    private Button mBtnSh;
    private Button mBtnQh;
    private Button mBtnXc;
    private Button mBtnSuining;
    private Button mBtnQinzhou;
    private Button mBtnNanning;
    private Button mBtnClear;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_map;
    }

    @Override
    protected void findViews() {
        mMapView = findViewById(R.id.mapView);
        mBtnBound = findViewById(R.id.btn_bound);
        mBtnDeleteMarker = findViewById(R.id.btn_delete_marker);
        mBtnOverlay = findViewById(R.id.btn_overlay);
        mBtnOnly = findViewById(R.id.btn_only);
        mEtCity = findViewById(R.id.et_city);
        mBtnOnly2 = findViewById(R.id.btn_only2);
        mBtnSc = findViewById(R.id.btn_sc);
        mBtnCq = findViewById(R.id.btn_cq);
        mBtnSh = findViewById(R.id.btn_sh);
        mBtnQh = findViewById(R.id.btn_qh);
        mBtnXc = findViewById(R.id.btn_xc);
        mBtnSuining = findViewById(R.id.btn_suining);
        mBtnQinzhou = findViewById(R.id.btn_qinzhou);
        mBtnNanning = findViewById(R.id.btn_nanning);
        mBtnClear = findViewById(R.id.btn_clear);
    }

    @Override
    protected void addListeners() {
        mBtnBound.setOnClickListener(v -> {
            VisibleRegion visibleRegion = mAMap.getProjection().getVisibleRegion();
            LatLngBounds latLngBounds = visibleRegion.latLngBounds;
            Log.d(TAG, "addListeners:" + latLngBounds.toString());
        });

        mBtnDeleteMarker.setOnClickListener(v -> {
            MarkerUtils.startGrowAnimationOut(marker, 1000, true);
        });
        mBtnOverlay.setOnClickListener(v -> {
            overMap();

        });
        //只显示一个省市的地图
        mBtnOnly2.setOnClickListener(v-> showProvince(mEtCity.getText().toString().trim()));
        mBtnSc.setOnClickListener(v-> showProvince("四川"));
        mBtnCq.setOnClickListener(v-> showProvince("重庆"));
        mBtnSh.setOnClickListener(v-> showProvince("上海"));
        mBtnQh.setOnClickListener(v-> showProvince("青海"));
        mBtnXc.setOnClickListener(v-> showProvince("西充"));
        mBtnSuining.setOnClickListener(v-> showProvince("遂宁"));
        mBtnQinzhou.setOnClickListener(v-> showProvince("钦州"));
        mBtnNanning.setOnClickListener(v-> showProvince("南宁"));
        mBtnClear.setOnClickListener(v-> {
            if(mMapShadowMaskHelper!=null){
                mMapShadowMaskHelper.clear();
            }

        });

    }

    /**
     * 省市名称/区域代码 省-市-区
     * @param province
     */
    private void showProvince(String province){
        if(mMapShadowMaskHelper==null){
            mMapShadowMaskHelper=new MapProvinceShowLimitHelper(this,this.getApplicationContext(),mAMap);
            mMapShadowMaskHelper.setBoundPadding(100)
                    .setMaskColor(Color.parseColor("#ffffff"))
                    .setStrokeColor(Color.parseColor("#eeeeee"))
                    .setBoundLatLngCountLimit(1000)
                    .setStrokeWidth(2);

        }
        mMapShadowMaskHelper.setCity(province);
        mMapShadowMaskHelper.show();


    }

    private void overMap(){
        List<LatLng> global;
        //绘制一个全世界的多边形

//            global = Arrays.asList(
//                    new LatLng(360, -179.9999999),
//                    new LatLng(360, 180),
//                    new LatLng(-360, 180),
//                    new LatLng(-360, -179.9999999)
//            );

        global = Arrays.asList(
                new LatLng(84.9, -179.9),
                new LatLng(84.9, 179.9),
                new LatLng(-84.9, 179.9),
                new LatLng(-84.9, -179.9)
        );

        Polygon polygon = mAMap.addPolygon(
                new PolygonOptions().addAll(global)
//                        .fillColor(Color.argb(150, 00, 00, 00))
                            .fillColor(Color.argb(200, 245, 245, 245))
                        .strokeColor(Color.RED)
                        .strokeWidth(3)
                        .zIndex(3));
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        aMapLifecycleHelper = new AMapLifecycleHelper(this, mMapView, savedInstanceState);
        mAMap = mMapView.getMap();

//        File filesDir = getFilesDir();
//        File styleFile = new File(filesDir, "amapstyle/style.data");
//        Utils.copyAssets(getApplicationContext(), "amap_style/style.data", styleFile.getAbsolutePath(),true);
//
//        File styleExtraFile = new File(filesDir, "amapstyle/style_extra.data");
//        Utils.copyAssets(getApplicationContext(), "amap_style/style_extra.data", styleExtraFile.getAbsolutePath(),true);

//        Log.d(TAG, "init:"+styleFile.getAbsolutePath()+",是否存在:"+styleFile.exists());
//        Log.d(TAG, "init:"+styleExtraFile.getAbsolutePath()+",是否存在:"+styleExtraFile.exists());

//        CustomMapStyleOptions customMapStyleOptions = new CustomMapStyleOptions();
//        customMapStyleOptions.setEnable(true)
//                .setStyleDataPath(styleFile.getAbsolutePath())
//                .setStyleExtraPath(styleExtraFile.getAbsolutePath())
//                .setStyleId("177701b713abda423af96204ca2b7df2");
//                .setStyleId("627681f385d76ed69c4761e46444e2d9");

//        mAMap.setCustomMapStyle(customMapStyleOptions);
        AMapHelper.setCustomMapStyle(mAMap,"177701b713abda423af96204ca2b7df2");
        AMapHelper.initSettings(mAMap);
        mAMap.moveCamera(CameraUpdateFactory.zoomTo(2f));


        MyLocationStyle myLocationStyle = new MyLocationStyleBuild()
                .setInterval(3000)
                .setRadiusFillColor(0x33ff00ff)
                .setStrokeColor(0x88ff0000)
                .setShowMyLocation(true)
                .setStrokeWidth(3)
                .build();
        MyLocationHelper.with(mAMap)
                .setMyLocationEnable(true)
                .setMyLocationStyle(myLocationStyle)
                .setMyLocationChangeListener(location -> Log.d(TAG, "init:" + location));


        MarkerOptions markerOptions = new MarkerOptionsBuild()
                .setIcon(R.drawable.icon_test)
                .setPosition(new LatLng(29.659257, 106.64614))
                .setAlpha(1.0f)
                .setInfoWindowEnable(true)
                .setTitle("我是标题")
                .setSnippet("我是描述")
                .setDraggable(true)
                .build();

        marker = mAMap.addMarker(markerOptions);
        MarkerUtils.startGrowAnimationIn(marker, 1000);


        mThreadPoolExecutor = new ThreadPoolExecutor(3,
                10,
                60, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100000)
        );
        mThreadPoolExecutor.allowCoreThreadTimeOut(true);

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (aMapLifecycleHelper != null) {
            aMapLifecycleHelper.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }







}
