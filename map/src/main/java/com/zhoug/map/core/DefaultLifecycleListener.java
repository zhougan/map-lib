package com.zhoug.map.core;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

/**
 * @Author 35574
 * @Date 2021/9/14
 * @Description
 */
public interface DefaultLifecycleListener extends ILifecycleListener {
    @Override
    default void onCreate(@NonNull LifecycleOwner owner) {

    }

    @Override
    default void onStart(@NonNull LifecycleOwner owner) {

    }

    @Override
    default void onResume(@NonNull LifecycleOwner owner) {

    }

    @Override
    default void onPause(@NonNull LifecycleOwner owner) {

    }

    @Override
    default void onStop(@NonNull LifecycleOwner owner) {

    }

    @Override
    default void onDestroy(@NonNull LifecycleOwner owner) {

    }

    @Override
    default void onStateChange(@NonNull LifecycleOwner owner, Lifecycle.Event event) {

    }

}
