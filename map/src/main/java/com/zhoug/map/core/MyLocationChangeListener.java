package com.zhoug.map.core;

import android.location.Location;

/**
 * 我的位置改变监听
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public interface MyLocationChangeListener {
    void onMyLocationChange(Location location);

}
