package com.zhoug.map.amap;

import com.amap.api.maps.AMap;
import com.amap.api.maps.model.MyLocationStyle;
import com.zhoug.map.core.MyLocationChangeListener;

import androidx.annotation.NonNull;

/**
 * 我的位置工具
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class MyLocationHelper {
    private AMap aMap;

    private MyLocationHelper(@NonNull AMap aMap) {
        this.aMap = aMap;
    }

    /**
     * 创建实例
     * @param amap
     * @return
     */
    public static MyLocationHelper with(@NonNull AMap amap){
        return new MyLocationHelper(amap);
    }

    /**
     * 是否开启我的位子
     * @param enable
     * @return
     */
    public MyLocationHelper setMyLocationEnable(boolean enable){
        aMap.setMyLocationEnabled(enable);
        return this;
    }

    /**
     * 设置我的位置样式
     * @param myLocationStyle 建议使用{@link MyLocationStyleBuild}创建,封装了默认样式
     * @return
     */
    public MyLocationHelper setMyLocationStyle(@NonNull MyLocationStyle myLocationStyle){
        aMap.setMyLocationStyle(myLocationStyle);
        return this;
    }

    /**
     * 我的位置改变监听
     * @param myLocationChangeListener
     * @return
     */
    public MyLocationHelper setMyLocationChangeListener(MyLocationChangeListener myLocationChangeListener){
        aMap.setOnMyLocationChangeListener(myLocationChangeListener::onMyLocationChange);
        return this;
    }






}
