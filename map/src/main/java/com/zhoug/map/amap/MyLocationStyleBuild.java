package com.zhoug.map.amap;

import android.graphics.Bitmap;
import android.view.View;

import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.MyLocationStyle;
import com.zhoug.map.R;

import androidx.annotation.DrawableRes;

/**
 * 我的位置样式创建工具
 *
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class MyLocationStyleBuild {
    /**
     * 设置定位类型
     * 默认设置,:定位位、且将视角移动到地图中心点,拖动后不会再次移动到中心,除非点击定位按钮
     */
    private int myLocationType = MyLocationStyle.LOCATION_TYPE_LOCATE;
    /**
     * 起定位请求的时间间
     * 默认只定位一次
     */
    private long interval = 500;
    /**
     * 定位（当前位置）的icon图标
     */
    private BitmapDescriptor locationIcon;
    /**
     * 定位图标的锚点X,相对图标的偏移量
     * 默认,左右居中
     */
    private float anchorX = 0.5f;
    /**
     * 定位图标的锚点Y,相对图标的偏移量
     * 默认,图标的最低点在位置中心
     */
    private float anchorY = 1.0f;
    /**
     * 设置圆形区域（以定位位置为圆心，定位半径的圆形区域）的填充颜色
     */
    private int radiusFillColor = -1;
    /**
     * 设置圆形区域（以定位位置为圆心，定位半径的圆形区域）的边框颜色
     */
    private int strokeColor = -1;

    /**
     * 设置圆形区域（以定位位置为圆心，定位半径的圆形区域）的边框宽度。
     */
    private float strokeWidth = -1;
    /**
     * 设置是否显示定位小蓝点(图标和圈)，true 显示，false不显示
     */
    private boolean myLocationVisible = true;

    public MyLocationStyleBuild() {
        //设置默认图标
        locationIcon = BitmapDescriptorFactory.fromResource(R.drawable.map_icon_location);
    }

    /**
     * 设置定位类型
     *
     * @param type {@link MyLocationStyle#LOCATION_TYPE_FOLLOW} 定位、且将视角移动到地图中心点，定位点跟随设备移动
     *             {@link MyLocationStyle#LOCATION_TYPE_LOCATE} 默认设置,  定位、且将视角移动到地图中心点,拖动后不会再次移动到中心,除非点击定位按钮
     *             {@link MyLocationStyle#LOCATION_TYPE_LOCATION_ROTATE} 定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动
     *             {@link MyLocationStyle#LOCATION_TYPE_MAP_ROTATE} 定位、且将视角移动到地图中心点，地图依照设备方向旋转，定位点会跟随设备移动
     *             {@link MyLocationStyle#LOCATION_TYPE_FOLLOW_NO_CENTER} 定位、但不会移动到地图中心点，并且会跟随设备移动
     *             {@link MyLocationStyle#LOCATION_TYPE_LOCATION_ROTATE_NO_CENTER} 定位、但不会移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动
     *             {@link MyLocationStyle#LOCATION_TYPE_MAP_ROTATE_NO_CENTER} 定位、但不会移动到地图中心点，地图依照设备方向旋转，并且会跟随设备移动
     *             {@link MyLocationStyle#LOCATION_TYPE_SHOW} 只定位
     * @return
     */
    public MyLocationStyleBuild setMyLocationType(int type) {
        myLocationType = type;
        return this;
    }

    /**
     * 设置发起定位请求的时间间隔，单位：毫秒，默认值：1000毫秒，如果传小于1000的任何值将执行单次定位
     *
     * @param millisecond
     * @return
     */
    public MyLocationStyleBuild setInterval(long millisecond) {
        this.interval = millisecond;
        return this;
    }

    /**
     * 设置定位（当前位置）的icon图标
     *
     * @param icon
     * @return
     */
    public MyLocationStyleBuild setLocationIcon(@DrawableRes int icon) {
        locationIcon = BitmapDescriptorFactory.fromResource(icon);
        return this;
    }

    /**
     * 设置定位（当前位置）的icon图标
     *
     * @param icon
     * @return
     */
    public MyLocationStyleBuild setLocationIcon(Bitmap icon) {
        locationIcon = BitmapDescriptorFactory.fromBitmap(icon);
        return this;
    }

    /**
     * 设置定位（当前位置）的icon图标
     *
     * @param assetPath 根据asset目录内资源名称 assets下的相对路径,例:icon/icon_test.png
     * @return
     */
    public MyLocationStyleBuild setLocationIconFromAsset(String assetPath) {
        locationIcon = BitmapDescriptorFactory.fromAsset(assetPath);
        return this;
    }

    /**
     * 设置定位（当前位置）的icon图标
     *
     * @param filePath 根据应用程序私有文件夹里包含文件的文件名
     * @return
     */
    public MyLocationStyleBuild setLocationIconFromFile(String filePath) {
        locationIcon = BitmapDescriptorFactory.fromFile(filePath);
        return this;
    }

    /**
     * 设置定位（当前位置）的icon图标
     *
     * @param absolutePath 根据图片文件的绝对地址
     * @return
     */
    public MyLocationStyleBuild setLocationIconFromPath(String absolutePath) {
        locationIcon = BitmapDescriptorFactory.fromPath(absolutePath);
        return this;
    }

    /**
     * 设置定位（当前位置）的icon图标
     *
     * @param view 根据一个 View
     * @return
     */
    public MyLocationStyleBuild setLocationIconFromView(View view) {
        locationIcon = BitmapDescriptorFactory.fromView(view);
        return this;
    }

    /**
     * 设置定位图标的锚点
     *
     * @param x 定位图标的锚点Y,相对图标的偏移量 默认,左右居中
     * @param y 定位图标的锚点Y,相对图标的偏移量 默认,图标的最低点在位置中心
     * @return
     */
    public MyLocationStyleBuild setAnchor(float x, float y) {
        this.anchorX = x;
        this.anchorY = y;
        return this;
    }

    /**
     * 设置定位图标的锚点图标的底部在位置中心
     *
     * @return
     */
    public MyLocationStyleBuild setAnchorVerticalBottom() {
        this.anchorY = 1.0f;
        return this;
    }

    /**
     * 设置定位图标的锚点图标的顶部在位置中心
     *
     * @return
     */
    public MyLocationStyleBuild setAnchorVerticalTop() {
        this.anchorY = 0.0f;
        return this;
    }

    /**
     * 设置定位图标的锚点图标的中心在位置中心
     *
     * @return
     */
    public MyLocationStyleBuild setAnchorVerticalCenter() {
        this.anchorY = 0.5f;
        return this;
    }


    /**
     * 设置圆形区域（以定位位置为圆心，定位半径的圆形区域）的填充颜色
     *
     * @param color
     * @return
     */
    public MyLocationStyleBuild setRadiusFillColor(int color) {
        this.radiusFillColor = color;
        return this;
    }

    /**
     * 设置圆形区域（以定位位置为圆心，定位半径的圆形区域）的边框颜色
     *
     * @param strokeColor
     * @return
     */
    public MyLocationStyleBuild setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
        return this;
    }

    /**
     * 设置是否显示定位小蓝点，true 显示，false不显示
     *
     * @param myLocationVisible
     * @return
     */
    public MyLocationStyleBuild setShowMyLocation(boolean myLocationVisible) {
        this.myLocationVisible = myLocationVisible;
        return this;
    }

    /**
     * 置圆形区域（以定位位置为圆心，定位半径的圆形区域）的边框宽度。
     *
     * @param strokeWidth
     * @return
     */
    public MyLocationStyleBuild setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        return this;
    }


    /**
     * 根据设置的属性创建MyLocationStyle
     * @return
     */
    public MyLocationStyle build() {
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationType(myLocationType);
        myLocationStyle.anchor(anchorX, anchorY);
        if (locationIcon != null) {
            myLocationStyle.myLocationIcon(locationIcon);
        }
        myLocationStyle.interval(interval);
        myLocationStyle.showMyLocation(myLocationVisible);
        if (radiusFillColor != -1) {
            myLocationStyle.radiusFillColor(radiusFillColor);
        }
        if (strokeColor != -1) {
            myLocationStyle.strokeColor(strokeColor);
        }
        if (strokeWidth != -1) {
            myLocationStyle.strokeWidth(strokeWidth);
        }
        return myLocationStyle;
    }


}
