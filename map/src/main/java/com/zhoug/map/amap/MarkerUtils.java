package com.zhoug.map.amap;

import android.view.animation.LinearInterpolator;

import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.animation.Animation;
import com.amap.api.maps.model.animation.ScaleAnimation;

/**
 * 高德地图Marker工厂
 * InfoWindow的样式可以用AMap.setInfoWindowAdapter方法设置,次接口的2个方法都返回null将不展示InfoWindow的信息
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class MarkerUtils {
    /**
     * 动画时长
     */
    private static  long ANIM_DURATION = 500;

    public static long getAnimDuration() {
        return ANIM_DURATION;
    }

    public static void setAnimDuration(long animDuration) {
        ANIM_DURATION = animDuration;
    }

    /**
     * 缩放动画
     *
     * @param marker
     * @param startScale 开始的缩放率
     * @param endScale   结束的缩放率
     * @param fillMode   状态模式 {@link Animation#FILL_MODE_FORWARDS 动画执行后保持在最后一帧 #FILL_MODE_BACKWARDS 动画执行后保持在第一帧}
     * @param duration   动画执行时间
     */
    public static void startScaleAnimation(Marker marker, float startScale, float endScale, int fillMode, long duration) {
        if (marker == null) return;
        Animation animation = new ScaleAnimation(startScale, endScale, startScale, endScale);
        animation.setInterpolator(new LinearInterpolator());
        //整个移动所需要的时间
        if(duration<=0){
            duration=ANIM_DURATION;
        }
        animation.setDuration(duration);
        //保持最后的状态
        animation.setFillMode(fillMode);
        //设置动画
        marker.setAnimation(animation);
        //开始动画
        marker.startAnimation();
    }

    /**
     * 缩放动画
     *
     * @param marker
     * @param startScale 开始的缩放率
     * @param endScale   结束的缩放率
     * @param fillMode   状态模式 {@link Animation#FILL_MODE_FORWARDS 动画执行后保持在最后一帧 #FILL_MODE_BACKWARDS 动画执行后保持在第一帧}
     */
    public static void startScaleAnimation(Marker marker, float startScale, float endScale, int fillMode) {
        startScaleAnimation(marker, startScale, endScale, fillMode, ANIM_DURATION);
    }

    /**
     * 开启marker生长动画(marker从地图上长出来)
     * @param marker
     */
    public static void startGrowAnimationIn(Marker marker, long duration) {
        startScaleAnimation(marker, 0, 1, Animation.FILL_MODE_FORWARDS, duration);
    }

    /**
     * 开启marker生长动画(marker从地图上变小小时)
     * @param marker
     * @param autoDestroy 是否在动画结束后自动删除当前marker并销毁Marker的图片等资源
     */
    public static void startGrowAnimationOut(Marker marker, long duration,boolean autoDestroy) {
        if(marker==null)return;
        if(autoDestroy){
            marker.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart() {
                }
                @Override
                public void onAnimationEnd() {
                    if(autoDestroy && marker!=null){
                        marker.destroy();
                    }
                }
            });
        }
        startScaleAnimation(marker, 1, 0, Animation.FILL_MODE_FORWARDS, duration);
    }

}
