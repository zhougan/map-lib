package com.zhoug.map.amap;

import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.CustomMapStyleOptions;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.zhoug.map.MapConstants;

import androidx.annotation.NonNull;

/**
 * 高德地图工具
 *
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class AMapHelper {
    private static final String TAG = ">>>AMapUtils";
    private static  boolean debug=false;


    /**
     * 地图ui设置
     *
     * @param aMap
     * @return
     */
    public static UiSettings initSettings(@NonNull AMap aMap) {
        UiSettings uiSettings = aMap.getUiSettings();
        //手势是否启用
        uiSettings.setRotateGesturesEnabled(false);//旋转是否启用
        uiSettings.setZoomGesturesEnabled(true);//缩放手势是否启用
        uiSettings.setGestureScaleByMapCenter(false);//是否以地图中心点缩放,一般设置false,以手指中心缩放
        uiSettings.setScrollGesturesEnabled(true); //滑动手势是否启用
        uiSettings.setTiltGesturesEnabled(false); //倾斜手势是否启用
        //地图上的控件是否显示
        uiSettings.setZoomControlsEnabled(false); //缩放按钮是否显示
        uiSettings.setZoomPosition(AMapOptions.ZOOM_POSITION_RIGHT_BUTTOM);//设置缩放按钮的位置
        uiSettings.setScaleControlsEnabled(true);// //比例尺是否显示
        uiSettings.setMyLocationButtonEnabled(true);//默认定位按钮是否显示
        uiSettings.setCompassEnabled(false);//指南针是否显示
        uiSettings.setIndoorSwitchEnabled(false);//室内地图楼层切换控件是否显示
//        uiSettings.setAllGesturesEnabled(true);//所有手势
        //logo设置,设置边距为负数达到隐藏logo的效果,但是比例尺也会被隐藏
        uiSettings.setLogoPosition(AMapOptions.LOGO_POSITION_BOTTOM_LEFT);//高德地图logo显示的位置
        uiSettings.setLogoLeftMargin(0);//Logo左边界距离屏幕左侧的边距
        uiSettings.setLogoBottomMargin(0);//Logo下边界距离屏幕底部的边距
        return uiSettings;
    }


    /**
     * 获取2点之间的距离
     * @param latLng1
     * @param latLng2
     * @return
     */
    public static float getDistance(LatLng latLng1, LatLng latLng2){
        return AMapUtils.calculateLineDistance(latLng1,latLng2);
    }

    /**
     * 把bounds给定的点全部显示在地图的可见区域
     * @param aMap
     * @param bounds {@link LatLngBounds.Builder}
     * @param boundMargin  四周留空宽度 px
     */
    public static void showBoundsInMap(AMap aMap, LatLngBounds bounds, int boundMargin, AMap.CancelableCallback callback){
        if(callback!=null){
            aMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, boundMargin),callback);
        }else{
            aMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, boundMargin));
        }
    }

    /**
     *  获取地图中心点(包含中心点坐标、缩放级别等)
     * @param aMap
     * @return
     */
    public static CameraPosition getMapCenter(AMap aMap){
        return aMap.getCameraPosition();
    }

    /**
     * 把给定的坐标移动到地图中心,动画移动
     * @param aMap
     * @param centerLatLng
     */
    public static void animateCenter(AMap aMap, LatLng centerLatLng, AMap.CancelableCallback cancelableCallback) {
        if(cancelableCallback!=null){
            aMap.animateCamera(CameraUpdateFactory.changeLatLng(centerLatLng),cancelableCallback);
        }else{
            aMap.animateCamera(CameraUpdateFactory.changeLatLng(centerLatLng));
        }
    }

    /**
     * 把给定的坐标移动到地图中心
     *
     * @param aMap
     * @param centerLatLng
     */
    public static void moveCenter(AMap aMap, LatLng centerLatLng) {
        aMap.moveCamera(CameraUpdateFactory.changeLatLng(centerLatLng));
    }

    /**
     * 把给定的坐标移动到地图中心并重置地图的缩放倍数
     *
     * @param aMap
     * @param centerLatLng
     * @param scale 地图缩放级别
     */
    public static void moveCenter(AMap aMap, LatLng centerLatLng, float scale) {
        aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(centerLatLng, scale));
    }

    /**
     * 限制地图的显示区域
     *
     * @param southwestLatLng 西南坐标
     * @param northeastLatLng 东北坐标
     */
    public static void setMapLimits(AMap aMap, LatLng southwestLatLng, LatLng northeastLatLng) {
        LatLngBounds latLngBounds = new LatLngBounds(southwestLatLng, northeastLatLng);
        aMap.setMapStatusLimits(latLngBounds);
    }

    /**
     * 设置自定义地图样式
     * @param aMap
     * @param styleId
     */
    public static void setCustomMapStyle(AMap aMap,String styleId){
        CustomMapStyleOptions customMapStyleOptions = new CustomMapStyleOptions();
        customMapStyleOptions.setEnable(true)
                .setStyleId(styleId);
        aMap.setCustomMapStyle(customMapStyleOptions);
    }

    /**
     * 设置限制地图显示,完全居中显示中国地图
     * @param aMap
     */
    public static void  setChinaMapLimits(AMap aMap){
        setMapLimits(aMap,new LatLng(-30.84585765086184,71.95051845009209),new LatLng(66.83321743742361,137.20682803509789));
    }

    /**
     * 中国地图边界
     * @return
     */
    public static LatLngBounds getChinaBounds(){
        double[] chinaNorthEast = MapConstants.getChinaNorthEast();
        double[] chinaSouthWest = MapConstants.getChinaSouthWest();
        return  new LatLngBounds(new LatLng(chinaSouthWest[0], chinaSouthWest[1]), new LatLng(chinaNorthEast[0], chinaNorthEast[1]));
    }


}
