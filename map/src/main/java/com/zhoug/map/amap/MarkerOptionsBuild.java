package com.zhoug.map.amap;

import android.graphics.Bitmap;

import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;

import androidx.annotation.DrawableRes;

/**
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class MarkerOptionsBuild {
    private MarkerOptions markerOptions;

    public MarkerOptionsBuild( ) {
        markerOptions=new MarkerOptions();
    }

    /**
     * 设置Marker覆盖物的锚点比例
     *
     * @param x 图标的锚点X,
     * @param y 图标的锚点Y,
     * @return
     */
    public MarkerOptionsBuild setAnchor(float x, float y) {
        markerOptions.anchor(x,y);
        return this;
    }

    /**
     * 设置Marker覆盖物的透明度
     * @param alpha
     * @return
     */
    public MarkerOptionsBuild setAlpha(float alpha) {
        markerOptions.alpha(alpha);
        return this;
    }

    /**
     * 设置Marker覆盖物是否可拖拽
     * @param draggable
     * @return
     */
    public MarkerOptionsBuild setDraggable(boolean draggable) {
        markerOptions.draggable(draggable);
        return this;
    }

    /**
     * 设置Marker覆盖物的图标
     * @param icon
     * @return
     */
    public MarkerOptionsBuild setIcon(BitmapDescriptor icon) {
        markerOptions.icon(icon);
        return this;
    }

    /**
     * 设置Marker覆盖物的图标
     * @param icon
     * @return
     */
    public MarkerOptionsBuild setIcon(@DrawableRes int icon) {
        markerOptions.icon(BitmapDescriptorFactory.fromResource(icon));
        return this;
    }

    /**
     * 设置Marker覆盖物的图标
     * @param icon
     * @return
     */
    public MarkerOptionsBuild setIcon(Bitmap icon) {
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        return this;
    }

    /**
     * 设置Marker覆盖物的InfoWindow是否允许显示
     * @param infoWindowEnable
     * @return
     */
    public MarkerOptionsBuild setInfoWindowEnable(boolean infoWindowEnable) {
        markerOptions.infoWindowEnable(infoWindowEnable);
        return this;
    }


    /**
     * 设置Marker覆盖物的位置坐标
     * @param position
     * @return
     */
    public MarkerOptionsBuild setPosition(LatLng position) {
        markerOptions.position(position);
        return this;
    }

    /**
     * 设置Marker覆盖物的图片旋转角度，从正北开始，逆时针计算。
     * @param rotateAngle
     * @return
     */
    public MarkerOptionsBuild setRotateAngle(float rotateAngle) {
        markerOptions.rotateAngle(rotateAngle);
        return this;
    }

    /**
     * 设置Marker覆盖物是否平贴地图
     * @param flat
     * @return
     */
    public MarkerOptionsBuild setFlat(boolean flat) {
        markerOptions.setFlat(flat);
        return this;
    }

    /**
     * 设置Marker覆盖物的InfoWindow相对Marker的偏移
     * @param infoWindowOffsetX
     * @return
     */
    public MarkerOptionsBuild setInfoWindowOffsetX(int infoWindowOffsetX, int infoWindowOffsetY) {
        markerOptions.setInfoWindowOffset(infoWindowOffsetX,infoWindowOffsetY);
        return this;
    }

    /**
     * 设置 Marker覆盖物的 文字描述
     * @param snippet
     * @return
     */
    public MarkerOptionsBuild setSnippet(String snippet) {
        markerOptions.snippet(snippet);
        return this;
    }

    /**
     * 设置 Marker覆盖物 的标题
     * @param title
     * @return
     */
    public MarkerOptionsBuild setTitle(String title) {
        markerOptions.title(title);
        return this;
    }

    /**
     * 设置Marker覆盖物是否可见
     * @param visible
     * @return
     */
    public MarkerOptionsBuild setVisible(boolean visible) {
        markerOptions.visible(visible);
        return this;
    }

    /**
     * 设置Marker覆盖物 zIndex。
     * @param zIndex
     * @return
     */
    public MarkerOptionsBuild setZIndex(float zIndex) {
        markerOptions.zIndex(zIndex);
        return this;
    }

    public MarkerOptions build(){
        return markerOptions;
    }


}
