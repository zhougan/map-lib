package com.zhoug.map.amap;

import android.content.Context;
import android.util.Log;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.district.DistrictItem;
import com.amap.api.services.district.DistrictResult;
import com.amap.api.services.district.DistrictSearch;
import com.amap.api.services.district.DistrictSearchQuery;
import com.zhoug.map.MapUtils;
import com.zhoug.map.data.database.MapDatabase;
import com.zhoug.map.data.database.dao.BoundaryDao;
import com.zhoug.map.data.database.dao.CityDao;
import com.zhoug.map.data.database.entities.Boundary;
import com.zhoug.map.data.database.entities.City;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 区域位置定位到地图显示范围上
 *
 * @Author 35574
 * @Date 2021/9/16
 * @Description
 */
public class MapAreaLocationHelper implements DistrictSearch.OnDistrictSearchListener, LifecycleObserver {
    private static final String TAG = ">>>MapProvinceShowLimit";
    private static boolean debug = false;
    private boolean destroy = false;
    private ThreadPoolExecutor mThreadPoolExecutor;
    private Context context;
    private Callback callback;
    private AMap aMap;
    /**
     * 需要显示地图的省市名称/区域代码
     * 最小支持到区县级
     */
    private String city;
    /**
     * 显示的地图距离边界的距离
     */
    private int boundPadding = 100;

    /**
     * 边界
     */
    private LatLngBounds latLngBounds;

    private boolean byParent;//如果设置的区域没有查询到边界则查询上级区域,直到查询成功

    public MapAreaLocationHelper(LifecycleOwner lifecycleOwner, @NonNull Context context, @NonNull AMap aMap) {
        this.context = context;
        this.aMap = aMap;
        if (lifecycleOwner != null) {
            lifecycleOwner.getLifecycle().addObserver(this);
        }
        mThreadPoolExecutor = new ThreadPoolExecutor(3,
                10,
                60, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100000)
        );
        mThreadPoolExecutor.allowCoreThreadTimeOut(true);
    }

    /**
     * 设置显示的省市名称/区域代码
     *
     * @param city
     * @return
     */
    public MapAreaLocationHelper setCity(String city) {
        this.city = city;
        return this;
    }

    /**
     * 显示的地图距离边界的距离
     *
     * @param boundPadding
     * @return
     */
    public MapAreaLocationHelper setBoundPadding(int boundPadding) {
        this.boundPadding = boundPadding;
        return this;
    }


    /**
     * 边界经纬度
     *
     * @return
     */
    public @Nullable
    LatLngBounds getLatLngBounds() {
        return latLngBounds;
    }


    private Disposable disposable;


    private void dispose() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            disposable = null;
        }
    }

    /**
     * 只显示指定省市的地图
     */
    public void show() {
        dispose();
        disposable = Observable
                .create((ObservableOnSubscribe<Boolean>) emitter -> {
                    emitter.onNext(searchDistrictLocal());
                    emitter.onComplete();
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(hasLocal -> {
                    if (!hasLocal) {
                        searchDistrictRemote();
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    searchDistrictRemote();
                });


    }

    /**
     * 调用接口获取边界
     *
     * @return
     */
    private void searchDistrictRemote() {
        if (debug) {
            Log.d(TAG, "调用接口获取边界");
        }
        DistrictSearch districtSearch = new DistrictSearch(context);
        DistrictSearchQuery query = new DistrictSearchQuery();
        query.setKeywords(city);
        query.setShowBoundary(true);
        query.setSubDistrict(0);//获取边界不需要返回下级行政区
        districtSearch.setQuery(query);
        districtSearch.setOnDistrictSearchListener(this);
        //请求边界数据 开始展示
        districtSearch.searchDistrictAsyn();
    }

    /**
     * 从本地数据库中查询边界
     */
    private boolean searchDistrictLocal() {
        CityDao cityDao = MapDatabase.getInstance().getCityDao();
        City c = cityDao.findByAreaCode(city);
        if (c == null) {
            c = cityDao.findByName(city);
        }
        if (c != null) {
            BoundaryDao boundaryDao = MapDatabase.getInstance().getBoundaryDao();
            List<Boundary> boundaries = boundaryDao.findByAreaCode(c.getAreaCode());
            if (boundaries != null && boundaries.size() > 0) {
                if (debug) {
                    Log.d(TAG, "从本地数据库中查询边界成功");
                }
                String[] bounds = new String[boundaries.size()];
                for (int i = 0; i < boundaries.size(); i++) {
                    bounds[i] = boundaries.get(i).getBound();
                }
                showCityBounds(bounds);
                return true;
            }
        }
        if (debug) {
            Log.d(TAG, "本地数据库没有存储边界:" + city);
        }
        return false;
    }

    @Override
    public void onDistrictSearched(DistrictResult districtResult) {
        if (destroy) {
            Log.e(TAG, "destroy");
            return;
        }
        if (districtResult == null) {
            Log.e(TAG, "districtResult is null ");
            if (callback != null) {
                callback.onFailure("没有获取到地图区域数据");
            }
            return;
        }
        if (districtResult.getAMapException() != null && districtResult.getAMapException().getErrorCode() == AMapException.CODE_AMAP_SUCCESS) {
            boolean success = false;
            ArrayList<DistrictItem> district = districtResult.getDistrict();
            if (district != null && district.size() > 0) {
                if (debug) {
                    Log.d(TAG, "district.size=" + district.size());
                }
                final DistrictItem districtItem = district.get(0);
                if (debug) {
                    Log.d(TAG, "districtItem=" + districtItem);
                }
                if (districtItem != null) {
                    final String[] bounds = districtItem.districtBoundary();
                    if (bounds == null || bounds.length == 0) {
                        Log.e(TAG, "bounds is empty");
                    } else {
                        success = true;
                        if (debug) {
                            Log.d(TAG, "bounds length:" + bounds.length);
                        }
                        //显示边界
                        mThreadPoolExecutor.execute(() -> showCityBounds(bounds));
                        //保存
                        mThreadPoolExecutor.execute(() -> keepCity(districtItem));
                    }
                } else {
                    Log.e(TAG, "districtItem is null");
                }

            } else {
                Log.e(TAG, "没有查询到区域");
            }
            if (!success) {
                if (byParent && MapUtils.isNumber(city)) {
                    String parentAreaCode = MapUtils.getParentAreaCode(city);
                    if(parentAreaCode!=null){
                        Log.d(TAG, "查询上级区域:"+parentAreaCode);
                        city=parentAreaCode;
                        show();
                    }else{
                        if (callback != null) {
                            callback.onFailure("没有获取到地图区域数据");
                        }
                    }
                } else {
                    if (callback != null) {
                        callback.onFailure("没有获取到地图区域数据");
                    }
                }
            }
        } else {
            Log.e(TAG, "发生错误");
            if (callback != null) {
                callback.onFailure("没有获取到地图区域数据");
            }
        }


    }

    /**
     * 把城市边界保存到数据库
     *
     * @param districtItem
     */
    private void keepCity(DistrictItem districtItem) {
        if (debug) {
            Log.d(TAG, "把城市边界保存到数据库");
        }
        //城市
        try {
            City city = new City();
            String adcode = districtItem.getAdcode();
            city.setAreaCode(adcode);
            city.setName(districtItem.getName());
            city.setLevel(districtItem.getLevel());
            LatLonPoint center = districtItem.getCenter();
            if (center != null) {
                city.setLatitude(center.getLatitude());
                city.setLongitude(center.getLongitude());
            }
            //城市边界
            final String[] bounds = districtItem.districtBoundary();
            List<Boundary> boundaries = new ArrayList<>();
            for (String bound : bounds) {
                boundaries.add(new Boundary(adcode, bound));
            }
            MapDatabase.getInstance().getCityDao().insert(city);
            MapDatabase.getInstance().getBoundaryDao().insertAll(boundaries);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 在地图可视范围内显示
     *
     * @param bounds
     */
    private void showCityBounds(String[] bounds) {
        if (debug) {
            Log.d(TAG, "showCityBounds");
        }
        long startTime = System.currentTimeMillis();
        latLngBounds = null;
        double left = 0;
        double right = 0;
        double top = 0;
        double bottom = 0;
        for (String bound : bounds) {
            if (debug) {
                Log.d(TAG, "bound:" + bound);
            }
            //bound: 106.076469,32.759056;106.076649,32.759653;....
            String[] lat = bound.split(";");
            if (lat.length > 0) {
                for (int i = 0; i < lat.length; i++) {
                    //106.076469,32.759056
                    String[] lats = lat[i].split(",");
                    double longitude = Double.parseDouble(lats[0]);//经度
                    double latitude = Double.parseDouble(lats[1]);//维度
                    if (top == 0 && bottom == 0 && left == 0 && right == 0) {
                        top = latitude;
                        bottom = latitude;
                        left = longitude;
                        right = longitude;
                    } else {
                        if (top < latitude) {
                            top = latitude;
                        }
                        if (bottom > latitude) {
                            bottom = latitude;
                        }
                        if (left > longitude) {
                            left = longitude;
                        }
                        if (right < longitude) {
                            right = longitude;
                        }
                    }
                }

            }
        }
        latLngBounds = LatLngBounds.builder()
                .include(new LatLng(bottom, left))
                .include(new LatLng(top, right))
                .build();
        if (debug) {
            Log.d(TAG, String.format(Locale.CANADA, "showCityBounds left:%f,top%f,right:%f,bottom:%f", left, top, right, bottom));
        }

        //把全部边界显示在地图上
        aMap.addOnCameraChangeListener(onCameraChangeListener);
        aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, boundPadding));

        long endTime = System.currentTimeMillis();
        if (debug) {
            Log.d(TAG, "耗时:" + (endTime - startTime));
        }
    }

    /**
     * 计算边界矩形坐标
     *
     * @param latLng
     */
    private void compareBound(@NonNull LatLng latLng) {
       /* if (top == 0 && bottom == 0 && left == 0 && right == 0) {
            top = latitude;
            bottom = latitude;
            left = longitude;
            right = longitude;
        } else {
            if (top < latitude) {
                top = latitude;
            }
            if (bottom > latitude) {
                bottom = latitude;
            }
            if (left > longitude) {
                left = longitude;
            }
            if (right < longitude) {
                right = longitude;
            }
        }*/
    }

    /**
     * 地图显示范围
     */
    private AMap.OnCameraChangeListener onCameraChangeListener = new AMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            Log.d(TAG, "onCameraChange:");
        }

        @Override
        public void onCameraChangeFinish(CameraPosition cameraPosition) {
            Log.d(TAG, "onCameraChangeFinish:");
            //限制地图显示
            if (aMap != null) {
                aMap.removeOnCameraChangeListener(onCameraChangeListener);
                if (callback != null) {
                    callback.onCameraChangeFinish(cameraPosition);
                }
            }

        }
    };


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy(@NonNull LifecycleOwner owner) {
        destroy = true;
        dispose();
        if (debug) {
            Log.d(TAG, "onDestroy");
        }
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setByParent(boolean byParent) {
        this.byParent = byParent;
    }

    public interface Callback {
        /**
         * 显示区域地图完成后,也可以自己在AMap.OnCameraChangeListener中监听
         *
         * @param cameraPosition
         */
        void onCameraChangeFinish(CameraPosition cameraPosition);

        /**
         * 显示区域地图失败
         *
         * @param error 失败,没有获取到区域边界,发生异常等错误信息
         */
        void onFailure(String error);
    }

}
