package com.zhoug.map;

/**
 * @Author 35574
 * @Date 2021/9/15
 * @Description
 */
public class MapConstants {

    /**
     * 中国的西南坐标
     * @return 维度,经度
     */
    public static double[] getChinaSouthWest(){
        return new double[]{3.51,73.33};
    }

    /**
     * 中国的东北坐标
     * @return 维度,经度
     */
    public static double[] getChinaNorthEast(){
        return new double[]{53.33,135.05};
    }

}
