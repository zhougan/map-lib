package com.zhoug.map;

import android.text.TextUtils;

import java.util.regex.Pattern;

/**
 * @Author: zhoug
 * @Date: 2023/8/18
 * @Description:
 */
public class MapUtils {
    /**
     * 获取上级区域的代码
     * eg: 450102001003->450102001
     *
     * @param areaCode 区域代码
     * @return 上级区域代码 null 没有上级区域了,上级区域为全国
     */
    public static String getParentAreaCode(String areaCode) {
        if (TextUtils.isEmpty(areaCode)) {
            return null;
        }
        //去掉最后的0
        areaCode = areaCode.replaceAll("0+$", "");
        //省和直辖市去掉0后只有2位,没有上级区域了
        if (areaCode.length() <= 2) {
            return null;
        }
        if (areaCode.length() <= 6) {
            areaCode = areaCode.substring(0, areaCode.length() - 2);
        } else {
            areaCode = areaCode.substring(0, areaCode.length() - 3);
        }
        while (areaCode.length() < 6) {
            areaCode = areaCode + "0";
        }

        return areaCode;
    }

    /**
     * 是否是数字
     * @param s
     * @return
     */
    public static boolean isNumber(String s) {
        if (TextUtils.isEmpty(s)) return false;
        return Pattern.compile("[0-9]+").matcher(s).matches();
    }
}
