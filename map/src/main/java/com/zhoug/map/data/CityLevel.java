package com.zhoug.map.data;

/**
 * 城市级别
 *
 * @Author 35574
 * @Date 2021/9/17
 * @Description
 */

public class CityLevel {
    public static final String PROVINCE_NAME = "province";
    public static final String CITY_NAME = "city";
    public static final String DISTRICT_NAME = "district";

    /**
     * 省/直辖市
     */
    public static final int PROVINCE = 1;
    /**
     * 市/市辖区
     */
    public static final int CITY = 2;
    /**
     * 城区/县
     */
    public static final int DISTRICT = 3;



}
