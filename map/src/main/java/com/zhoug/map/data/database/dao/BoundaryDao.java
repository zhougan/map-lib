package com.zhoug.map.data.database.dao;

import com.zhoug.map.data.database.entities.Boundary;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

/**
 * @Author 35574
 * @Date 2021/9/17
 * @Description
 */
@Dao
public interface BoundaryDao {

    /**
     * 插入城市边界
     * @param boundaries
     */
    @Insert
    void insertAll(List<Boundary> boundaries);

    /**
     * 根据区域代码查询城市
     * @param areaCode
     * @return
     */
    @Query("select * from Boundary where areaCode = :areaCode")
    List<Boundary>  findByAreaCode(String areaCode);

    
}
