package com.zhoug.map.data.database.entities;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * 城市
 * @Author 35574
 * @Date 2021/9/17
 * @Description select * from City where areaCode='450100'
 */
@Keep
@Entity
public class City {

    /**
     * 区域地址代码:450100
     */
    @NonNull
    @PrimaryKey
    private String areaCode;
    /**
     * 城市名字:南宁市
     */
    private String name;
    /**
     * 城市中心维度:22.8177
     */
    private double latitude;
    /**
     * 城市中心经度:108.366407
     */
    private double longitude;

    /**城市级别
     * province:省/直辖市
     * city:市/市辖区
     * district:城区/县
     */
    private String level;//

    @NonNull
    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(@NonNull String areaCode) {
        this.areaCode = areaCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
