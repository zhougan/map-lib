package com.zhoug.map.data.database.dao;

import com.zhoug.map.data.database.entities.City;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

/**
 * @Author 35574
 * @Date 2021/9/17
 * @Description
 */
@Dao
public interface CityDao {

    /**
     * 插入城市
     * @param city
     */
    @Insert
    void insert(City city);

    /**
     * 根据区域代码查询城市
     * @param areaCode
     * @return
     */
    @Query("select * from City where areaCode = :areaCode")
    City findByAreaCode(String areaCode);
    /**
     * 根据名称代码查询城市
     * @param name
     * @return
     */
    @Query("select * from City where name like '%'||:name||'%'")
    City findByName(String name);

}
