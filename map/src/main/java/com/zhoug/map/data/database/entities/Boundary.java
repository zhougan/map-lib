package com.zhoug.map.data.database.entities;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/**
 * 城市分界线,一个城市可以有多天分界线
 * @Author 35574
 * @Date 2021/9/17
 * @Description select * from Boundary where areaCode='450100'
 */
@Keep
@Entity
public class Boundary {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long id;
    /**
     * 城市区域代码
     */
    private String areaCode;
    /**
     * 分界线的经纬度集合:106.076469,32.759056;106.076649,32.759653;....
     */
    private String bound;

    public Boundary() {
    }

    @Ignore
    public Boundary(String areaCode, String bound) {
        this.areaCode = areaCode;
        this.bound = bound;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getBound() {
        return bound;
    }

    public void setBound(String bound) {
        this.bound = bound;
    }
}
