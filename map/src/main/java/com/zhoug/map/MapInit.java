package com.zhoug.map;

import android.app.Application;

import com.zhoug.map.data.database.MapDatabase;

/**
 * 地图初始化工具
 * @Author 35574
 * @Date 2021/9/14
 * @Description
 */
public class MapInit {

    private static final int MAP_TYPE_BAIDU = 1;
    private static final int MAP_TYPE_GAODE = 2;
    private static int mapType = MAP_TYPE_GAODE;

    public static void init(Application application) {
        MapDatabase.init(application);
    }


}
